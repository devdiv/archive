package nl.inquisitive.mosar.flashcard.helpers.readandstrippdf;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class StripISTQBPDFTest {

    @Test
    public void StripFileTest_AssertsEquals_IfStripIsSuccessful(){
        //create testdata
        String testData = "acceptance criteria\n" +
                "Ref: ISO 24765\n" +
                "The criteria that a component or system must satisfy in order to be accepted by a user, customer, or other authorized entity.\n";
        //create object
        StripISTQBPDF stripISTQBPDF = new StripISTQBPDF(testData);

        //assert object has correct return data
        Assertions.assertEquals("acceptance criteria",stripISTQBPDF.getAllTermsList().get(0));
        Assertions.assertEquals("The criteria that a component or system must satisfy in order to be accepted by a user, customer, or other authorized entity.",stripISTQBPDF.getAllExplanationsList().get(0));
    }

}