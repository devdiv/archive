package nl.inquisitive.mosar.flashcard.helpers.readandstrippdf;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;
import java.io.IOException;

@Slf4j
public class ReadISTQBPDF {

    @Getter
    @Setter
    String allTextOFDocumentExceptFrontPage;

    public ReadISTQBPDF(String pdfFileName) throws IOException {
        log.debug("Starting read istqb file");
        //read pdf file
        File file = new File(pdfFileName);
        //return read pdf file pd.document
        PDDocument internalPDDocument = (PDDocument.load(file));
        //strip PDF of stuff
        PDFTextStripper pdfTextStripper = new PDFTextStripper();
        //set staring page skip  the first
        pdfTextStripper.setStartPage(2);
        //to string
        setAllTextOFDocumentExceptFrontPage(pdfTextStripper.getText(internalPDDocument));
        //close reader
        internalPDDocument.close();
        log.debug("Ending read istqb file");
    }
}
